const crypto = require('crypto');
const axios = require('axios');
const fs = require('fs');
const URL = require('url').URL;
const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

const fuckedUpFunction = (key, secret, resource, workspace, docData, filePath = 'C:\\Users\\Antonio.Mihalache\\Desktop\\file.pdf') => {
	const msg = `${key}${resource}${workspace}`;
	const hmac = crypto.createHmac("sha256", secret);
	hmac.update(msg);
	const signature = hmac.digest().toString("hex");
	const headers = {
		"X-Auth-Key": key,
		"X-Auth-Workspace": workspace,
		"X-Auth-Signature": signature,
		"Accept": "application/json",
		"Content-Type": "application/json; charset=utf-8"
	};
	return axios.post(new URL(resource, "https://pdfgeneratorapi.com/api/v3/").href, docData, {
		headers
	})
		.then(responseJSON => {
			const pdfBuffer = Buffer.from(responseJSON.data.response, "base64");
			if (filePath) {
				fs.writeFileSync(filePath, pdfBuffer);
			}
			return pdfBuffer;
		});
};


app.get('/', (req, res) => {
	fuckedUpFunction(
		"9e35152e427062d3fff9bf5f7504c531c117383a85e77580e12d56baf3c490c2",
		"a974866559df2414a18669c6d161a5a3b1b169f621fffcc3f67d1b314f28281f",
		"templates/21650/output",
		"flo@yopmail.com",
		[
			{
				"TxnDate": "01-01-2001",
				"DueDate": "02-02.2001",
				"DocNumber": 10,
				"BillEmail": {
					"Address": "kaki@kaki.com"
				},
				"CustomerInfo": {
					"GivenName": "Gigi",
					"FamilyName": "Kent"
				},
				"items": [
					{
						"id": 1,
						"name": "Item one"
					}
				]
			}
		]

	)
		.then(result => {
			console.log(result);
			res.send({ success: true, message: 'Ti s-a descarcat pdf-u pe desktop'})
		})
		.catch(err => console.log(err));

});


app.listen(port, () => console.log('Server up'))